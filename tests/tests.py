import sys


def test_resource_monitor():
    args = ['-p', 'AETIONOMY', '--config_file', '.xnat.cfg']
    sys.path.append('./bin')
    from resource_monitor import create_parser, resource_monitor
    parser = create_parser()
    args = parser.parse_args(args)
    resource_monitor(args)

    args = ['-p', 'SUBFIELDS', '--config_file', '.xnat.cfg']
    sys.path.append('./bin')
    from resource_monitor import create_parser, resource_monitor
    parser = create_parser()
    args = parser.parse_args(args)
    resource_monitor(args)
