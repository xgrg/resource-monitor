# resource-monitor

[![pipeline status](https://gitlab.com/xgrg/resource-monitor/badges/master/pipeline.svg)](https://gitlab.com/xgrg/resource-monitor/commits/master)
[![coverage report](https://gitlab.com/xgrg/resource-monitor/badges/master/coverage.svg)](https://gitlab.com/xgrg/resource-monitor/commits/master)

Monitors incoming additions of resources of a given type (e.g. outputs from
FreeSurfer, FSL BET, etc.) and sends notifications to a given Slack channel
