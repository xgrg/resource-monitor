#!/usr/bin/env python
import logging as log
from datetime import datetime
import json
import os
startTime = datetime.now()  # not nice but avoids passing it everywhere

DEBUG_MODE = True


def notify_slack(text, attachment, webhook_url, debug_mode=False):

    def md5(fname):
        import hashlib
        hash_md5 = hashlib.md5()
        with open(fname, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        return hash_md5.hexdigest()

    # Estimates elapsed time
    try:
        seconds = datetime.now() - startTime
        m, s = divmod(seconds.total_seconds(), 60)
        h, m = divmod(m, 60)
        elapsedtime = "%d:%02d:%02d" % (h, m, s)
    except NameError:
        elapsedtime = 'n/a'

    # Sends the report to Slack
    payload = {'text': "%s Elapsed time: %s. (resource-monitor v.%s)"
                       % (text, elapsedtime, md5(__file__)[:8]),
               'attachments': [{'fallback': 'XNAT stats',
                                'text': attachment,
                                'mrkdwn_in': ['text', 'pretext']}],
               'link_names': 1,
               'mrkdwn': True,
               'username': 'resource-monitor',
               'icon_emoji': ':computer:'}

    if not debug_mode:
        payload.update({'channel': '#xnat'})
    else:
        payload.update({'channel': '@goperto'})

    payload = json.dumps(payload).replace('"', '\\"').replace('\n', '\\n')

    cmd = 'curl -H "Content-Type:application/json" --data "%s" %s'\
        % (payload, webhook_url)
    log.info(cmd)

    os.system(cmd)


def status_monitor(args):
    import requests
    log.basicConfig(level=log.INFO)
    url = args.url

    if args.verbose:
        log.getLogger().setLevel(log.INFO)
    else:
        log.getLogger().setLevel(log.WARNING)

    text = 'Current status of the following services:'
    attachment = ''

    urls = json.load(open(args.url))
    for n, url in urls.items():
        try:
            ans = requests.get(url)
            ans = ans.ok
        except requests.ConnectionError:
            ans = False
        emoji = ':white_check_mark:' if ans else ':x:'
        attachment += '%s _%s_ (%s)\n' % (emoji, n, url)
    return text, attachment


def create_parser():
    import argparse

    desc = 'Performs a GET request to a URL and check if the answer is 200'
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("--config_file", help="Configuration file",
                        default='/home/grg/.xnat_bsc.cfg')
    parser.add_argument("--url", help='JSON file containing the URLs')
    msg = 'Display verbosal information (optional)'
    parser.add_argument('--verbose', '-V', action='store_true', default=False,
                        help=msg, required=False)
    return parser


if __name__ == '__main__':
    try:
        parser = create_parser()
        args = parser.parse_args()
        text, attachment = status_monitor(args)

    except Exception as e:
        # Default message in case of failures
        text, attachment = ['The control process has resulted in an error.',
                            str(e)]

    # Sends notification to Slack
    cfg = json.load(open(args.config_file))
    if 'slack_webhook' in cfg.keys():
        webhook_url = cfg['slack_webhook']
        notify_slack(text, attachment, webhook_url, debug_mode=DEBUG_MODE)
    else:
        log.warning('No slack webhook provided. Failing gracefully.')

    log.info((text, attachment))
