#!/usr/bin/env python
import logging as log
from datetime import datetime
import json
import pyxnat
import os
startTime = datetime.now()  # not nice but avoids passing it everywhere


def notify_slack(text, attachment, webhook_url, debug_mode=False):

    def md5(fname):
        import hashlib
        hash_md5 = hashlib.md5()
        with open(fname, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        return hash_md5.hexdigest()

    # Estimates elapsed time
    try:
        seconds = datetime.now() - startTime
        m, s = divmod(seconds.total_seconds(), 60)
        h, m = divmod(m, 60)
        elapsedtime = "%d:%02d:%02d" % (h, m, s)
    except NameError:
        elapsedtime = 'n/a'

    # Sends the report to Slack
    payload = {'text': "%s Elapsed time: %s. (resource-monitor v.%s)"
                       % (text, elapsedtime, md5(__file__)[:8]),
               'attachments': [{'fallback': 'XNAT stats',
                                'text': attachment,
                                'mrkdwn_in': ['text', 'pretext']}],
               'link_names': 1,
               'mrkdwn': True,
               'username': 'resource-monitor',
               'icon_emoji': ':computer:'}

    if not debug_mode:
        payload.update({'channel': '#xnat-monitors'})
    else:
        payload.update({'channel': '@goperto'})

    payload = json.dumps(payload).replace('"', '\\"').replace('\n', '\\n')

    cmd = 'curl -H "Content-Type:application/json" --data "%s" %s'\
        % (payload, webhook_url)
    log.info(cmd)

    os.system(cmd)


def resource_monitor(args):

    log.basicConfig(level=log.INFO)
    config_file = args.config_file
    resource_name = args.resource

    if args.verbose:
        log.getLogger().setLevel(log.INFO)
    else:
        log.getLogger().setLevel(log.WARNING)

    log.info('* config file: %s' % config_file)

    c = pyxnat.Interface(config=config_file)
    if args.project is not None:
        projects = [c.select.project(args.project)]
    else:
        projects = list(c.select.projects())
    log.info(projects)

    resources = {}
    n_res = {}
    n_exp = {}

    for p in projects:
        experiments = [e['ID'] for e in c._get_json(p._uri + '/experiments')]

        n_res[p] = 0
        resources[p] = []
        n_exp[p] = 0

        for e in experiments:
            e = c.select.experiment(e)
            data = c._get_json(e.resource(resource_name)._uri + '/files')
            if len(data) != 0:
                resources[p].append(e)
                n_res[p] = n_res[p] + 1
            n_exp[p] = n_exp[p] + 1

    n_total_res = sum(n_res.values())
    n_total_exp = sum(n_exp.values())

    pc = 0.0
    if n_total_exp > 0:
        pc = n_total_res / float(n_total_exp) * 100.0
    text = 'There are currently %s resources in %s projects on XNAT (%s)'\
        ' of type *%s* over %s existing sessions (%.1f%%)'\
        % (n_total_res, len(n_res.keys()), c._server, resource_name,
           n_total_exp, pc)

    attachment = ''
    for p in projects:
        if n_exp[p] == 0 or n_res[p] == 0:
            continue
        pc = n_res[p] / float(n_exp[p]) * 100.0
        attachment += '*%s*: _%s resources over %s sessions (%.1f%%)_\n'\
            % (p.label(), n_res[p], n_exp[p], pc)
    return text, attachment


def create_parser():
    import argparse

    desc = 'Monitors incoming additions of resources of a given type (e.g.'\
        ' outputs from FreeSurfer, FSL BET, etc.) and sends notifications '\
        'to a given Slack channel.'
    parser = argparse.ArgumentParser(description=desc)

    parser.add_argument("--config_file", help="Configuration file",
                        default='/home/grg/.xnat_bsc.cfg')
    parser.add_argument("--resource", help="Resource name",
                        default='FREESURFER6')
    parser.add_argument("-p", "--project", help="Project name", default=None)
    msg = 'Display verbosal information (optional)'
    parser.add_argument('--verbose', '-V', action='store_true', default=False,
                        help=msg, required=False)
    return parser


if __name__ == '__main__':
    try:
        parser = create_parser()
        args = parser.parse_args()
        text, attachment = resource_monitor(args)

    except Exception as e:
        # Default message in case of failures
        text, attachment = ['The control process has resulted in an error.',
                            str(e)]

    # Sends notification to Slack
    cfg = json.load(open(args.config_file))
    if 'slack_webhook' in cfg.keys():
        webhook_url = cfg['slack_webhook']
        notify_slack(text, attachment, webhook_url, debug_mode=False)
    else:
        log.warning('No slack webhook provided. Failing gracefully.')

    log.info((text, attachment))
